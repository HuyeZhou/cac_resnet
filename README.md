# Resnet for CAC score prediction

## Requirements
Tensorflow 2.X
sklearn
imblearn
pandas
numpy

## 4-level prediction
cac_resnet.py

## 2-level prediction
cac_resnet_binary.py

## note
both scripts performing oversampling for small subgroups.