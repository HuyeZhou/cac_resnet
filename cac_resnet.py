import os
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
import tensorflow as tf
import pandas as pd
from glob import glob
import numpy as np
import pathlib
from sklearn.model_selection import train_test_split
from tensorflow import keras
from tensorflow.keras import layers
from imblearn.over_sampling import RandomOverSampler
from tensorflow.keras import regularizers
import tensorflow_addons as tfa
devices_id = 2
epochs= 200
lr= 1e-4
l2= 1e-3
logdir= 'output/resnet50/logs'
checkdir= 'output/resnet50/checkpoints'
gpus = tf.config.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        tf.config.experimental.set_visible_devices(gpus[devices_id], 'GPU')
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
    except RuntimeError as e:
        print(e)

def cac_map(c):
    if c<=10:
        return 0.0
    elif 10<c<=100:
        return 1.0
    elif 100<c<=400:
        return 2.0
    else:
        return 3.0

data_augmentation = tf.keras.Sequential([
    tf.keras.layers.experimental.preprocessing.RandomFlip("horizontal"),
    tf.keras.layers.experimental.preprocessing.RandomRotation(0.3),
    tf.keras.layers.experimental.preprocessing.RandomZoom((-0.2,0.2))
    ])
AUTOTUNE = tf.data.experimental.AUTOTUNE

def process(file_path, label):
    label = tf.reshape(label, [-1])
    image = tf.io.read_file(file_path)
    image = tf.image.decode_png(image, channels=1)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.per_image_standardization(image)
    image = tf.image.resize(image, [2048, 1600])
    return image, label

def prepare(ds, shuffle=False, augment=False):
    if shuffle:
        ds = ds.shuffle(1000)
    ds = ds.map(process, num_parallel_calls=AUTOTUNE).batch(4)
    if augment:
        ds = ds.map(lambda x,y: (data_augmentation(x, training=True), y), 
        num_parallel_calls=AUTOTUNE)
    return ds.prefetch(buffer_size=AUTOTUNE)


def res_identity(x, filters): 
  x_skip = x 
  f1, f2 = filters

  x = layers.Conv2D(f1, kernel_size=(1, 1), strides=(1, 1), padding='valid', kernel_regularizer=regularizers.l2(l2))(x)
  x = layers.BatchNormalization()(x)
  x = layers.Activation(tf.keras.activations.relu)(x)

  x = layers.Conv2D(f1, kernel_size=(3, 3), strides=(1, 1), padding='same', kernel_regularizer=regularizers.l2(l2))(x)
  x = layers.BatchNormalization()(x)
  x = layers.Activation(tf.keras.activations.relu)(x)

  x = layers.Conv2D(f2, kernel_size=(1, 1), strides=(1, 1), padding='valid', kernel_regularizer=regularizers.l2(l2))(x)
  x = layers.BatchNormalization()(x)
  x = layers.Add()([x, x_skip])
  x = layers.Activation(tf.keras.activations.relu)(x)

  return x


def res_conv(x, s, filters):
  x_skip = x
  f1, f2 = filters

  x = layers.Conv2D(f1, kernel_size=(1, 1), strides=(s, s), padding='valid', kernel_regularizer=regularizers.l2(l2))(x)
  x = layers.BatchNormalization()(x)
  x = layers.Activation(tf.keras.activations.relu)(x)

  x = layers.Conv2D(f1, kernel_size=(3, 3), strides=(1, 1), padding='same', kernel_regularizer=regularizers.l2(l2))(x)
  x = layers.BatchNormalization()(x)
  x = layers.Activation(tf.keras.activations.relu)(x)

  x = layers.Conv2D(f2, kernel_size=(1, 1), strides=(1, 1), padding='valid', kernel_regularizer=regularizers.l2(l2))(x)
  x = layers.BatchNormalization()(x)

  x_skip = layers.Conv2D(f2, kernel_size=(1, 1), strides=(s, s), padding='valid', kernel_regularizer=regularizers.l2(l2))(x_skip)
  x_skip = layers.BatchNormalization()(x_skip)
  x = layers.Add()([x, x_skip])
  x = layers.Activation(tf.keras.activations.relu)(x)

  return x


def resnet50():

  input_im = layers.Input(shape=(2048, 1600, 1))
  x = layers.ZeroPadding2D(padding=(3, 3))(input_im)

  x = layers.Conv2D(64, kernel_size=(7, 7), strides=(2, 2))(x)
  x = layers.BatchNormalization()(x)
  x = layers.Activation(tf.keras.activations.relu)(x)
  x = layers.MaxPooling2D((3, 3), strides=(2, 2))(x)

  x = res_conv(x, s=1, filters=(64, 256))
  x = res_identity(x, filters=(64, 256))
  x = res_identity(x, filters=(64, 256))

  x = res_conv(x, s=2, filters=(128, 512))
  x = res_identity(x, filters=(128, 512))
  x = res_identity(x, filters=(128, 512))
  x = res_identity(x, filters=(128, 512))

  x = res_conv(x, s=2, filters=(256, 1024))
  x = res_identity(x, filters=(256, 1024))
  x = res_identity(x, filters=(256, 1024))
  x = res_identity(x, filters=(256, 1024))
  x = res_identity(x, filters=(256, 1024))
  x = res_identity(x, filters=(256, 1024))

  x = res_conv(x, s=2, filters=(512, 2048))
  x = res_identity(x, filters=(512, 2048))
  x = res_identity(x, filters=(512, 2048))

  x = layers.AveragePooling2D((2, 2), padding='same')(x)
  x = layers.Flatten()(x)
  x = layers.Dense(4, activation='softmax', kernel_initializer='he_normal')(x)

  model = tf.keras.Model(inputs=input_im, outputs=x, name='Resnet50')

  return model


def main():
    df = pd.read_csv("data/Hoagh-Baker-BAC-CAC-prospective_data_updated.csv")
    df = df.reindex(columns=["patient_id", "Bradley-Score", "CAC score ", "Age", "accession_number", "Filename"])
    df = df.rename({"CAC score ":"cac"}, axis='columns')
    df = df.dropna()
    df['cac_bins'] = df['cac'].map(lambda x: cac_map(x))
    df2 = pd.read_csv("data/JHU-OLD-UCSD-UPDATED-BAC_sopuid.csv")
    df2 = df2.dropna()
    df2['cac_bins'] = df2['CAC_new_score'].map(lambda x:cac_map(x))
    image_dir = "/nfs/experiments/Anirudh_cmAngio_2020/cmangio_updated/cmangio_2020/Hoagh-Data-CAC-BAC-close/{}/PNGs_OriginalSize/{}.png" 
    file_dir = []
    y = []
    for _,row in df.iterrows():
        file_dir.append(image_dir.format(row.patient_id,row.Filename))
        y.append(row.cac_bins)
    for _,row in df2.iterrows():
        file_dir.append(row.sopuid+".png")
        y.append(row.cac_bins)
    files_train, files_test, y_train, y_test = train_test_split(file_dir, y, test_size=0.3, stratify=y, random_state=24)
    ros = RandomOverSampler(random_state=42)
    files_train_res, y_train_res = ros.fit_resample(np.reshape(files_train, (-1, 1)), y_train)
    files_train_res = files_train_res.reshape(-1)
    list_ds_train = tf.data.Dataset.from_tensor_slices((files_train_res,y_train_res))
    list_ds_test = tf.data.Dataset.from_tensor_slices((files_test,y_test))
    train_ds = prepare(list_ds_train, shuffle=True, augment=True)
    test_ds = prepare(list_ds_test)
    model = resnet50()
    lr_schedule = tf.keras.optimizers.schedules.InverseTimeDecay(lr,
                                                                 decay_steps=174*10,
                                                                 decay_rate=1,
                                                                 staircase=False)
    model.compile(optimizer=keras.optimizers.Adam(lr_schedule),
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=False),
                  metrics=[tfa.metrics.CohenKappa(num_classes=4, weightage = 'quadratic',sparse_labels=True)])
    model.fit(
        train_ds,
        validation_data=test_ds,
        epochs=epochs,
        callbacks=[tf.keras.callbacks.TensorBoard(logdir + '/fit/{}_{}_{}'.format(lr, l2, epochs),
                                                  profile_batch='4, 8'
                                                  ),
                   tf.keras.callbacks.ModelCheckpoint(filepath=checkdir + '/{}_{}_{}'.format(lr, l2, epochs),
                                                      monitor='val_loss', mode='min', save_weights_only=True, save_best_only=True
                                                      ),
                   tf.keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=epochs // 4,
                                                    mode='min'
                                                    )
                   ]
        )


if __name__ == "__main__":
    main()